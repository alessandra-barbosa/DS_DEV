# "Star Jeans" -  Product pricing project based on competition

### Business problem
What is the best selling price for pants? 

### Questions to be answered:
What is the best selling price for the pants?How many types of pants and their colors for the initial product?What are the raw materials needed to make the pants?

### Data source
- H&M: https://www2.hm.com/en_us/men/products/jeans.html
- Macys: https://www.macys.com/shop/mens-clothing/mens-jeans
